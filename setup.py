from setuptools import setup, find_packages

setup(
    name='pip_package_for_Giessmann_et_al_2019',
    version='0.0.1',
    packages=find_packages(exclude=['test', 'test.*']),
    include_package_data=False,
    platforms='any',
    install_requires=[
        'gitpython',
        'mbdoe',
        'data_toolbox',
        'natsort',
        'json_tricks'
    ]
)
