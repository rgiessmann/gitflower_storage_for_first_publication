####################### header version 2019-03-25 ####################
import hashlib
import io
import os
import sys

def sha256sum(src, digest_size_in_bytes=64, length=io.DEFAULT_BUFFER_SIZE):
    if sys.hexversion <= 0x030500f0:
        ## digest_size not settable before Python 3.5
        if digest_size_in_bytes != 64:
            print("Not possible to hash with digest_size different than 64 with your Python version.")
            sys.exit(2)
        sha2 = hashlib.new("sha256")
    else:
        sha2 = hashlib.new("sha256", digest_size=digest_size_in_bytes)
    with io.open(src, mode="rb") as fd:
        for chunk in iter(lambda: fd.read(length), b''):
            sha2.update(chunk)
    return sha2.hexdigest()

_mypath = os.path.abspath(__file__)
_mysha256sum = sha256sum(_mypath)
_myfilename = os.path.basename(__file__)

import logging
logger = logging.getLogger(__name__)

logFormatter = logging.Formatter("%(asctime)s [%(levelname)-5.5s]  %(message)s")
rootLogger = logging.getLogger()
rootLogger.setLevel(logging.INFO)

fileHandler = logging.FileHandler("{}.{}.log".format(_myfilename, sha256sum(_mypath)[:6]), mode="w")
fileHandler.setFormatter(logFormatter)
fileHandler.setLevel(logging.INFO)
rootLogger.addHandler(fileHandler)

consoleHandler = logging.StreamHandler()
consoleHandler.setFormatter(logFormatter)
consoleHandler.setLevel(logging.INFO)
rootLogger.addHandler(consoleHandler)


logger.info("Hi, this is: '{}' as '{}'".format(_myfilename, __name__))
logger.info("I am located here:")
logger.info(_mypath)
logger.info("My sha256sum hash is:")
logger.info(_mysha256sum)
######################################################################


import os

print("I am here:")
print(os.getcwd())



#!/usr/bin/env python2
# -*- coding: utf-8 -*-


print("I am here:")
print(os.getcwd())
os.chdir("./files/02__extract_keq")
print("changed to:")
print(os.getcwd())
###

import pandas
import glob

file_to_check = "./metadata.csv"
if os.path.exists(file_to_check):
    os.remove(file_to_check)
os.symlink(os.path.abspath("../01__convert_to_excel/data/metadata_for_parameter_estimation.csv"), file_to_check)

if not os.path.exists("./data/"):
    os.mkdir("./data/")
for file_to_link in glob.glob("../01__convert_to_excel/data/*.xls"):
    file_to_link = os.path.abspath(file_to_link)
    only_basename_of_file = os.path.basename(file_to_link)
    file_to_check = "./data/{}".format(only_basename_of_file)
    if os.path.exists(file_to_check):
        os.remove(file_to_check)
    os.symlink(file_to_link, file_to_check)


import importlib.util
spec = importlib.util.spec_from_file_location("mbdoe", "./mbdoe.py")
mbdoe = importlib.util.module_from_spec(spec)
spec.loader.exec_module(mbdoe)

mbdoe.enable_debug()

metadata="metadata.csv"
suite = mbdoe.SuiteOfExperimentalData()
suite.read_experiments(metadata)

tmp_df_list = []
for (metadata, expdata) in suite.all_experiments:
    initial_nucl1_concentration = metadata["nucl1"]
    initial_enz1_concentration  = metadata["enz1"]
    initial_base1_concentration = metadata["base1"]
    initial_r1p_concentration   = metadata["R1P"]
    initial_phos_concentration  = metadata["phos"]

    final_nucl1_concentration    = expdata.as_dataframe()["nucl1"]
    final_base1_concentration   = initial_base1_concentration + (initial_nucl1_concentration - final_nucl1_concentration)
    final_r1p_concentration     = initial_r1p_concentration   + (initial_nucl1_concentration - final_nucl1_concentration)
    final_phos_concentration    = initial_phos_concentration  - (initial_nucl1_concentration - final_nucl1_concentration)


    tmp_df = pandas.DataFrame()

    tmp_df["eq_nucl1"]      =   final_nucl1_concentration
    tmp_df["eq_base1"]      =   final_base1_concentration
    tmp_df["eq_r1p"]        =   final_r1p_concentration
    tmp_df["eq_phos"]       =   final_phos_concentration

    tmp_df["initial_nucl1"] = initial_nucl1_concentration
    tmp_df["initial_enz1"]  = initial_enz1_concentration
    tmp_df["initial_base1"] = initial_base1_concentration
    tmp_df["initial_r1p"]   = initial_r1p_concentration
    tmp_df["initial_phos"]  = initial_phos_concentration

    tmp_df["K_eq"]          = final_base1_concentration * final_r1p_concentration / final_nucl1_concentration / final_phos_concentration

    tmp_df_list.append(tmp_df)

df = pandas.concat(tmp_df_list, ignore_index=True)
df.to_csv("out.csv")


df = pandas.read_csv("out.csv")

import matplotlib.pyplot as plt

fig = plt.figure(figsize=(8 /2.54, 12 /2.54), dpi=600)
ax = plt.subplot(211)
ax.set_yscale("log")

plt.subplots_adjust(left=0.3)


plt.scatter(df.index, df["K_eq"], c=df["initial_phos"])

#plt.scatter(df["initial_phos"], df["K_eq"])
cbar = plt.colorbar()
cbar.set_label("init. phosphate conc. [mM]")
plt.xlabel("experiment number ")
plt.ylabel("equilibrium constant")
plt.axhline(y=0.10, linestyle="--")
#ax.set_ylim(ymin=0.1)
plt.xlim(-5,85)
ax.set_yticks([0.02, 0.1, 1])
#ax.set_yticks([.2, .3, .4, .5, .6, .7, .8, .9,], minor=True)
ax.text(53, 0.115, r"$K_{eq}$ = 0.10", fontsize=8)
#.xticks([])
ax.set_xticks([0,20,40])
ax.set_xticks([10,30,50,60,70,80], minor=True)
ax.text(0,-1, "Figure 5 Giessmann et al.", transform=ax.transAxes)


fig.savefig("Figure 5 Giessmann et al.pdf")
fig.savefig("Figure 5 Giessmann et al.png")

plt.close(fig)

print("K_eq as median:")
print(df["K_eq"].median())
###
print("I am here:")
print(os.getcwd())
os.chdir("../../")
print("changed to:")
print(os.getcwd())
