####################### header version 2019-03-25 ####################
import hashlib
import io
import os
import sys

def sha256sum(src, digest_size_in_bytes=64, length=io.DEFAULT_BUFFER_SIZE):
    if sys.hexversion <= 0x030500f0:
        ## digest_size not settable before Python 3.5
        if digest_size_in_bytes != 64:
            print("Not possible to hash with digest_size different than 64 with your Python version.")
            sys.exit(2)
        sha2 = hashlib.new("sha256")
    else:
        sha2 = hashlib.new("sha256", digest_size=digest_size_in_bytes)
    with io.open(src, mode="rb") as fd:
        for chunk in iter(lambda: fd.read(length), b''):
            sha2.update(chunk)
    return sha2.hexdigest()

_mypath = os.path.abspath(__file__)
_mysha256sum = sha256sum(_mypath)
_myfilename = os.path.basename(__file__)

import logging
logger = logging.getLogger(__name__)

logFormatter = logging.Formatter("%(asctime)s [%(levelname)-5.5s]  %(message)s")
rootLogger = logging.getLogger()
rootLogger.setLevel(logging.INFO)

fileHandler = logging.FileHandler("{}.{}.log".format(_myfilename, sha256sum(_mypath)[:6]), mode="w")
fileHandler.setFormatter(logFormatter)
fileHandler.setLevel(logging.INFO)
rootLogger.addHandler(fileHandler)

consoleHandler = logging.StreamHandler()
consoleHandler.setFormatter(logFormatter)
consoleHandler.setLevel(logging.INFO)
rootLogger.addHandler(consoleHandler)


logger.info("Hi, this is: '{}' as '{}'".format(_myfilename, __name__))
logger.info("I am located here:")
logger.info(_mypath)
logger.info("My sha256sum hash is:")
logger.info(_mysha256sum)
######################################################################


import os


logger = logging.getLogger(__name__)



print("I am here:")
print(os.getcwd())

os.chdir("./files/01__convert_to_excel/")

print("...changed to:")
print(os.getcwd())




logger.info("Reading metadata and generated csv files...")

import pandas as pd
meta_data = pd.read_csv("../00__fit_raw_spectral_data/metadata.csv")
meta_data = meta_data.dropna(axis='index', how='all')

import glob

import os
try:
    os.mkdir("./data")
except FileExistsError as e:
    pass

metadata_for_parameter_estimation = pd.read_csv("./metadata-template-for-parameter-estimation.csv", skiprows=1)
metadata_for_parameter_estimation = metadata_for_parameter_estimation.dropna(axis='index', how='all')

collate_metadata_for_parameter_estimation_list = []

all_conc_files = glob.glob("../00__fit_raw_spectral_data/csv_files/07_concentrations_*.csv")
for index, concentration_file in enumerate(all_conc_files):
    logger.info(f"Working through file {index+1} of {len(all_conc_files)}...")
    df = pd.read_csv(concentration_file)
    condition_name = df.Condition_Name.unique()
    assert(len(condition_name) == 1)
    condition_name = condition_name[0]


    ## check if this file is needed for Parameter Estination
    _query = f"description == '{condition_name}'"
    pe_meta_for_this_condition = metadata_for_parameter_estimation.query(_query)
    if len(pe_meta_for_this_condition) == 0:
        ## file is not needed
        continue
    elif len(pe_meta_for_this_condition) == 1:
        ## file is good, go for it!
        pass
    else:
        ## more than one entry. What the fuck?!
        raise

    how_many_infinity_rows = 10
    collate_output_df_list = []
    row_counter = 0
    for _, i in df.iterrows():
        timepoint = i.Time_Point

        _query = f"Condition_Name == '{condition_name}' & Time_Point == {timepoint}"

        metadata_of_current_entry = meta_data.query(_query)
        assert(len(metadata_of_current_entry == 1))

        values_of_current_entry = df.query(_query)
        assert(len(values_of_current_entry == 1))

        if int(metadata_of_current_entry.Include_In_Parameter_Estimation) != True:
            continue
        else:
            row_counter += 1

        ## rewrite values into excel based format
        total_conc_at_this_timepoint = float(metadata_of_current_entry.Total_Concentration)
        substrate_at_this_timepoint = float(values_of_current_entry.deoxythymidine)
        variance_substrate_at_this_timepoint = float(values_of_current_entry.stderr_deoxythymidine)**2
        #variance_substrate_at_this_timepoint = 0.1* substrate_at_this_timepoint

        output_df = pd.DataFrame(index=[0],columns=["Time_Point_in_min", "Substrate_in_mM", "Variance_Substrate_in_mM"]+[f"infinity_{i}" for i in range(how_many_infinity_rows)])
        output_df["Time_Point_in_min"] = timepoint
        output_df["Substrate_in_mM"] = substrate_at_this_timepoint
        output_df["Variance_Substrate_in_mM"] = variance_substrate_at_this_timepoint
        output_df[[f"infinity_{i}" for i in range(how_many_infinity_rows)]] = float("infinity")
        collate_output_df_list.append(output_df)


    output_df = pd.concat(collate_output_df_list, ignore_index=True)
    #print(output_df)
    output_df.to_excel(f"./data/data_{condition_name}.xls")


    ## rearrange metadata

    meta_tmp_df = pe_meta_for_this_condition.copy()
    meta_tmp_df["filename"] = f"./data/data_{condition_name}.xls"

    which_real_data_columns = ["t", "nucl1", "variance_nucl1"]
    which_infinity_variances = ["variance_phos","variance_enz1", "variance_enz1_comp_ENP","variance_base1","variance_R1P"]

    meta_tmp_df["names_of_data_columns"] = repr(which_real_data_columns + which_infinity_variances)

    def construct_data_column_range(number_of_rows):
        import string
        foobar = ""
        assert(number_of_rows <= 25)
        for i in range(number_of_rows):
            ## starts counting from "B"
            foobar += string.ascii_uppercase[i+1] + ","
        foobar = foobar[:-1]
        return repr(foobar)

    how_many_rows = len(which_real_data_columns) + len(which_infinity_variances)
    column_names = construct_data_column_range(how_many_rows)

    meta_tmp_df["data_is_in_columns"] = column_names

    meta_tmp_df["data_length_in_rows"] = len(collate_output_df_list)

    collate_metadata_for_parameter_estimation_list.append(meta_tmp_df)

meta_df = pd.concat(collate_metadata_for_parameter_estimation_list, ignore_index=True)
meta_df = meta_df.sort_values(by=["Entry"])

meta_df.to_csv("./data/metadata_for_parameter_estimation.csv")
content_2 = ""
with open("./data/metadata_for_parameter_estimation.csv", "r") as f:
    content_2 = f.read()


with open("./data/metadata_for_parameter_estimation.csv", "w") as f:
    f.write(",\n")
    f.write(content_2)

logger.info("... Done!")



print("I am here:")
print(os.getcwd())

os.chdir("../../")

print("...changed to:")
print(os.getcwd())
