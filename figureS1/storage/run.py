####################### header version 2019-04-03 ####################
import hashlib
import io
import os
import sys
import shutil

def sha256sum(src, digest_size_in_bytes=64, length=io.DEFAULT_BUFFER_SIZE):
    if sys.hexversion <= 0x030500f0:
        ## digest_size not settable before Python 3.5
        if digest_size_in_bytes != 64:
            print("Not possible to hash with digest_size different than 64 with your Python version.")
            sys.exit(2)
        sha2 = hashlib.new("sha256")
    else:
        sha2 = hashlib.new("sha256", digest_size=digest_size_in_bytes)
    with io.open(src, mode="rb") as fd:
        for chunk in iter(lambda: fd.read(length), b''):
            sha2.update(chunk)
    return sha2.hexdigest()

_mypath = os.path.abspath(__file__)
_mysha256sum = sha256sum(_mypath)
_myfilename = os.path.basename(__file__)

shutil.copyfile(_mypath, "{}.{}".format(_myfilename, sha256sum(_mypath)[:6]))

import logging
logger = logging.getLogger(__name__)

logFormatter = logging.Formatter("%(asctime)s [%(levelname)-5.5s]  %(message)s")
rootLogger = logging.getLogger()
rootLogger.setLevel(logging.INFO)

fileHandler = logging.FileHandler("{}.{}.log".format(_myfilename, sha256sum(_mypath)[:6]), mode="w")
fileHandler.setFormatter(logFormatter)
fileHandler.setLevel(logging.INFO)
rootLogger.addHandler(fileHandler)

consoleHandler = logging.StreamHandler()
consoleHandler.setFormatter(logFormatter)
consoleHandler.setLevel(logging.INFO)
rootLogger.addHandler(consoleHandler)


logger.info("Hi, this is: '{}' as '{}'".format(_myfilename, __name__))
logger.info("I am located here:")
logger.info(_mypath)
logger.info("My sha256sum hash is:")
logger.info(_mysha256sum)
######################################################################


import importlib.util
spec = importlib.util.spec_from_file_location("data_toolbox", "./files/data_toolbox.py")
data_toolbox = importlib.util.module_from_spec(spec)
spec.loader.exec_module(data_toolbox)


import os

print("I am here:")
print(os.getcwd())

os.chdir("./files/")

print("...changed to:")
print(os.getcwd())

data = data_toolbox.Data()
data.parse_args(argv=["-s", "-v", "-m", "metadata.csv"])

data.read_data()
data.sub_background()
data.plot_data()
data.rearrange_data()
data.group_data()
data.plot_all_timepoints()
data.conversion_rate_from_twopoint_calibration()
data.store_individual_concentration_values()
data.plot_conversion_rates()





################################################################################
### top plot
################################################################################


import pandas as pd
df = pd.read_csv("csv_files/03_df_mean.csv")
df = df.dropna(how="all")

normalize_wavelength=277

for unique_sample_id in df.Unique_Sample_ID.unique():
    _query = f"Unique_Sample_ID == '{unique_sample_id}' & Wavelength == {normalize_wavelength}"
    normalize_divisor = float(df.query(_query).Abs)
    row_selector = df.Unique_Sample_ID == unique_sample_id
    df.loc[row_selector, "Abs"] = df.loc[ row_selector , "Abs"] / normalize_divisor

df.to_csv("normalized.csv")


import matplotlib.pyplot as plt

plt.style.use("default")

fig = plt.figure(figsize=(16 /2.54, 20 /2.54), dpi=600)

#ax = plt.subplot(311)
ax = plt.subplot2grid((3, 3), (0, 0), colspan=3)

plt.subplots_adjust(left=0.25, hspace=0.5)

for unique_sample_id in df.Unique_Sample_ID.unique():
    print(unique_sample_id)
    row_selector = df.Unique_Sample_ID == unique_sample_id
    col_selector = ["Wavelength", "Abs"]
    _data = df.loc[row_selector, col_selector]
    _label = df.loc[row_selector].Description.unique()[0]
    import numpy
    _x = _data.Wavelength
    _y = _data.Abs

    ax.errorbar(x=_x, y=_y, label=_label) #], "-", color="black", linewidth=4) # label="ideal")

plt.xlim((250, 350))
plt.ylim((-0.1, 1.5))

#plt.errorbar(x=df.mole_fraction, y=df.deoxythymidine, yerr=1.96 * df.stderr_deoxythymidine, markersize=5, fmt="s", capsize=8) # label="predicted"

ax.set_xlabel("Wavelength [nm]")
ax.set_ylabel("rel. Absorbance [A.U.]")
#plt.title(
#    f"Normalized absorbances of Thymidine / Thymine mixtures. \n \
#    Wavelength for normalization = {normalize_wavelength}. \n \
#    Arrow direction = direction of increasing Thymine (product) concentration.", fontsize=8)


min_290nm = df.loc[df.query("Wavelength == 290").Abs.idxmin()]
max_290nm = df.loc[df.query("Wavelength == 290").Abs.idxmax()]

#print(min_290nm)

ax.annotate("", xy=(290, max_290nm.Abs), xytext=(290, min_290nm.Abs+ 0.1*(max_290nm.Abs-min_290nm.Abs) ), arrowprops=dict(width=9, headwidth=24))
#plt.text(290, (min_290nm.Abs + 0.5*(max_290nm.Abs-min_290nm.Abs)), "increasing product, i.e. thymine")



min_265nm = df.loc[df.query("Wavelength == 265").Abs.idxmin()]
max_265nm = df.loc[df.query("Wavelength == 265").Abs.idxmax()]

ax.annotate("", xy=(265, min_265nm.Abs ), xytext=( 265, min_265nm.Abs+ 0.95*(max_265nm.Abs-min_265nm.Abs) ), arrowprops=dict(width=9, headwidth=24))
#plt.text(265, (min_265nm.Abs + 0.5*(max_265nm.Abs-min_265nm.Abs)), "increasing product, i.e. thymine")

ax.vlines(277, -1, 2, linewidths=1, zorder=20)
ax.vlines(300, -1, 2, linewidths=1, zorder=20)





################################################################################
### middle plot
################################################################################


import pandas as pd
df = pd.read_csv("csv_files/08_conv_rates.csv")
df = df.dropna()
def return_educt_concentration_from_Condition_Name(cond_name):
    return float(cond_name.split("_dT_")[0])
df["educt_concentration"] = df.Condition_Name.apply(return_educt_concentration_from_Condition_Name)
df["mole_fraction"] = ( df.educt_concentration / 100 )

print(df)


import statsmodels.formula.api as smf
model = smf.ols(formula='deoxythymidine ~ mole_fraction -1', data=df).fit()
print(model.summary())


import matplotlib.pyplot as plt
import numpy as np



#ax = fig.add_subplot(312)
ax = plt.subplot2grid((3, 3), (1, 0), colspan=2)

plt.plot([0,1],[0,1], "-", color="black", linewidth=2) # label="ideal")
plt.xlim((-0.1, 1.1))
plt.ylim((-0.1, 1.1))

plt.errorbar(x=df.mole_fraction, y=df.deoxythymidine, yerr=None, markersize=5, fmt="s", capsize=8) # label="predicted"

plt.xlabel("actual mole fraction deoxythymidine")
plt.ylabel("predicted")

ax.set_xticks([0,.2,.4,.6,.8,1.0])
ax.set_yticks([0,.2,.4,.6,.8,1.0])
#ax.grid(which="major", axis="both")

for i in [0.0, 0.2, 0.4, 0.6, 0.8, 1.0]:
    ax.hlines(i, -0.2, i, linewidths=1, colors="grey")
    ax.vlines(i, -0.2, i, linewidths=1, colors="grey")

#plt.title(
#    f"Predicted mole fraction of Thymidine from spectral fits \n \
#    against true mole fractions. Error bars = 95%CI of fit.", fontsize=12)
#

ax.text(1.2, 0.8, f"R² = {round(model.rsquared,3)}", horizontalalignment="left", bbox=dict(facecolor='grey', alpha=0.5))




################################################################################
### bottom plot
################################################################################



#ax = fig.add_subplot(313)
ax = plt.subplot2grid((3, 3), (2, 0), colspan=2)

plt.xlim([-0.1,+1.1])
plt.ylim([-10,+10])

_xdata = list(df.mole_fraction)
_ydata = list( (df.deoxythymidine - df.mole_fraction)*100 )

print(_ydata)

plt.plot(_xdata, _ydata, "rs")#, fmt="s")

plt.xlabel("actual mole fraction deoxythymidine")
plt.ylabel("residual [pp]")
#plt.title(
#    f"Residuals of (predicted - actual) mole fractions for  \n \
#      Thymidine / Thymine mixtures. Error in percentage points.", fontsize=12)

ax.hlines(+5, -0.2, 1.2, linestyles="dashed")
ax.hlines(-5, -0.2, 1.2, linestyles="dashed")


fig.savefig("Figure S1 Giessmann et al.pdf")
fig.savefig("Figure S1 Giessmann et al.png")
plt.close("all")


########
