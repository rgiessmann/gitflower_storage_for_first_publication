####################### header version 2019-03-25 ####################
import hashlib
import io
import os
import sys

def sha256sum(src, digest_size_in_bytes=64, length=io.DEFAULT_BUFFER_SIZE):
    if sys.hexversion <= 0x030500f0:
        ## digest_size not settable before Python 3.5
        if digest_size_in_bytes != 64:
            print("Not possible to hash with digest_size different than 64 with your Python version.")
            sys.exit(2)
        sha2 = hashlib.new("sha256")
    else:
        sha2 = hashlib.new("sha256", digest_size=digest_size_in_bytes)
    with io.open(src, mode="rb") as fd:
        for chunk in iter(lambda: fd.read(length), b''):
            sha2.update(chunk)
    return sha2.hexdigest()

_mypath = os.path.abspath(__file__)
_mysha256sum = sha256sum(_mypath)
_myfilename = os.path.basename(__file__)

import logging
logger = logging.getLogger(__name__)

logFormatter = logging.Formatter("%(asctime)s [%(levelname)-5.5s]  %(message)s")
rootLogger = logging.getLogger()
rootLogger.setLevel(logging.INFO)

fileHandler = logging.FileHandler("{}.{}.log".format(_myfilename, sha256sum(_mypath)[:6]), mode="w")
fileHandler.setFormatter(logFormatter)
fileHandler.setLevel(logging.INFO)
rootLogger.addHandler(fileHandler)

consoleHandler = logging.StreamHandler()
consoleHandler.setFormatter(logFormatter)
consoleHandler.setLevel(logging.INFO)
rootLogger.addHandler(consoleHandler)


logger.info("Hi, this is: '{}' as '{}'".format(_myfilename, __name__))
logger.info("I am located here:")
logger.info(_mypath)
logger.info("My sha256sum hash is:")
logger.info(_mysha256sum)
######################################################################

import os

print("I am here:")
print(os.getcwd())

os.chdir("./files/")

print("...changed to:")
print(os.getcwd())




import pandas


df = pandas.read_csv("metadata-template-for-parameter-estimation.csv", skiprows=1)
df = df[["description_1", "nucl1", "phos", "enz1"]].rename(mapper = {"description_1": "Exp. Condition #", "nucl1": "Nucleoside / mM", "phos" : "Phosphate / mM", "enz1": "Enzyme / mM"}, axis="columns")


# Create a Pandas Excel writer using XlsxWriter as the engine.
writer = pandas.ExcelWriter("Table 1 Giessmann et al.xlsx", engine='xlsxwriter')

# Convert the dataframe to an XlsxWriter Excel object.
df.to_excel(writer, sheet_name='Sheet1')

# Get the xlsxwriter workbook and worksheet objects.
workbook  = writer.book
worksheet = writer.sheets['Sheet1']

# Add some cell formats.
format1 = workbook.add_format({'num_format': '#,##0.0000'})

# Set the format but not the column width.
worksheet.set_column('E:E', None, format1)

# Close the Pandas Excel writer and output the Excel file.
writer.save()

writer.close()


##########

print("I am here:")
print(os.getcwd())

os.chdir("../")

print("...changed to:")
print(os.getcwd())




###
