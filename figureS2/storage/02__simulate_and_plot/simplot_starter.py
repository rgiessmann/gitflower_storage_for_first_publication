#!/usr/bin/env python2
# -*- coding: utf-8 -*-

import os

print("I am here:")
print(os.getcwd())
os.chdir("./files/02__simulate_and_plot/")
print("changed to:")
print(os.getcwd())



import shutil
shutil.copy("../01__convert_to_excel/data/metadata_for_parameter_estimation.csv", "./metadata.csv")
shutil.copytree("../01__convert_to_excel/data", "data")

import subprocess
subprocess.run('chmod u+x simplot_starter.sh', shell=True)
subprocess.run('/bin/bash -c "./simplot_starter.sh"', shell=True)


print("I am here:")
print(os.getcwd())
os.chdir("../../")
print("changed to:")
print(os.getcwd())
