####################### header version 2019-03-25 ####################
import hashlib
import io
import os
import sys

def sha256sum(src, digest_size_in_bytes=64, length=io.DEFAULT_BUFFER_SIZE):
    if sys.hexversion <= 0x030500f0:
        ## digest_size not settable before Python 3.5
        if digest_size_in_bytes != 64:
            print("Not possible to hash with digest_size different than 64 with your Python version.")
            sys.exit(2)
        sha2 = hashlib.new("sha256")
    else:
        sha2 = hashlib.new("sha256", digest_size=digest_size_in_bytes)
    with io.open(src, mode="rb") as fd:
        for chunk in iter(lambda: fd.read(length), b''):
            sha2.update(chunk)
    return sha2.hexdigest()

_mypath = os.path.abspath(__file__)
_mysha256sum = sha256sum(_mypath)
_myfilename = os.path.basename(__file__)

import logging
logger = logging.getLogger(__name__)

logFormatter = logging.Formatter("%(asctime)s [%(levelname)-5.5s]  %(message)s")
rootLogger = logging.getLogger()
rootLogger.setLevel(logging.INFO)

fileHandler = logging.FileHandler("{}.{}.log".format(_myfilename, sha256sum(_mypath)[:6]), mode="w")
fileHandler.setFormatter(logFormatter)
fileHandler.setLevel(logging.INFO)
rootLogger.addHandler(fileHandler)

consoleHandler = logging.StreamHandler()
consoleHandler.setFormatter(logFormatter)
consoleHandler.setLevel(logging.INFO)
rootLogger.addHandler(consoleHandler)


logger.info("Hi, this is: '{}' as '{}'".format(_myfilename, __name__))
logger.info("I am located here:")
logger.info(_mypath)
logger.info("My sha256sum hash is:")
logger.info(_mysha256sum)
######################################################################

import os

logger = logging.getLogger(__name__)

logger.info("Starting the plotting...")



import json_tricks as pickler
import matplotlib
import matplotlib.pyplot as plt
import numpy
import mbdoe
import model as mod
import numpy
import copy
import math

from collections import OrderedDict

# Enabling debug mode for diagnosis
mbdoe.enable_debug()

plt.style.use("default")



extract_params = ["k_E_N_P", "k_E_N_P_2", "k_ENP_D", "k_ENP_D_2"]

parameter_estimation_results_file = "results.json"



m = mod.initialize()
opt = mbdoe.Optimization(m, True)
opt.set_solver_sundials()

with open(parameter_estimation_results_file, "r") as f:
    parameter_values = OrderedDict(pickler.load(f))

for key in extract_params:
    setattr(opt.model, key, parameter_values[key])



suite = mbdoe.SuiteOfExperimentalData()
suite.read_experiments("metadata.csv")
suite.calculate_and_store_all_modelled_data(opt)


number_of_experiments = len(suite.all_experiments)

print("number_of_experiments")
print( number_of_experiments )

import math
foo = math.ceil(number_of_experiments / 4)

fig, axes = plt.subplots(nrows=int(foo), ncols=4, sharex = True, sharey = True, figsize=(16 /2.54, foo*4 /2.54), dpi=600)
plt.tight_layout()
plt.subplots_adjust(bottom=0.1, left=0.15)

#########
## plot right graph
#########

_how_many_experiments_to_plot = number_of_experiments

how_many_outside_95ci = 0
i = 0
x = 0
y = 0
for (_meta_exp, experimental_data), modelled_data in zip(suite.all_experiments, suite.all_experiments_modelled):
    print(i,x,y)

    _c = list(matplotlib.colors.to_rgba("black"))
    ax = axes[x,y]

    mod_df = modelled_data.as_dataframe()
    mod_df["t"] /=60

    ax.errorbar(x=mod_df["t"], y=mod_df["nucl1"], yerr=None                , fmt="-", label="", c=_c, linewidth=3)

    _c = list(matplotlib.colors.to_rgba("red"))
    _lighter_color = copy.copy(_c)
    _lighter_color[3] = 0.5

    exp_df = experimental_data.as_dataframe()
    exp_df["95ci_error"] = 1.96* exp_df["variance_nucl1"].apply(math.sqrt)
    exp_df["t"] = exp_df.index
    exp_df["t"] /= 60

    n = str(_meta_exp["nucl1"])
    e = str(_meta_exp["enz1"])
    p = str(_meta_exp["phos"])
    d = str(_meta_exp["description_1"])
    ax.text(5, 5, d )

    ax.errorbar(x=exp_df["t"], y=exp_df["nucl1"], yerr=exp_df["95ci_error"], fmt="s", label=_meta_exp["description"], c=_c, markersize=4, capsize=3, ecolor=_lighter_color, markeredgecolor="black", markeredgewidth=0.5, elinewidth=2, linewidth=0)

    for i, exp in exp_df.iterrows():
        closest_mod = mod_df.iloc[(mod_df['t']-exp["t"]).abs().argsort()[:1]]
        diff_to_exp = closest_mod["nucl1"] - exp["nucl1"]
        diff_in_95cis = abs(diff_to_exp) / exp["95ci_error"]
        print(diff_in_95cis)
        if diff_in_95cis.iloc[0] > 1:
            how_many_outside_95ci += 1

    ax.set_yticks([0,1,2,3,4,5])

    ax.set_xticks([0,8,16,24])
    ax.set_xticks([0,2,4,6,8,10,12,14,16,18,20,22,24], minor=True)

    plt.ylim(0,6)

    i += 1
    if y == 3:
        y = 0
        x += 1
    else:
        y += 1


print("how_many_outside_95ci:")
print(how_many_outside_95ci)

plt.text(0.05,0.5,"nucleoside conc. [mM]", rotation="vertical", transform=fig.transFigure)
plt.text(0.5,0.06,"time [h]", transform=fig.transFigure)

plt.text(0.4, 0.2, "Figure S2 Giessmann et al.", transform=fig.transFigure)

fig.savefig("Figure S2 Giessmann et al.pdf")
fig.savefig("Figure S2 Giessmann et al.png")
###
