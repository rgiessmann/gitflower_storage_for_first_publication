#!/bin/bash

#-S --- set the desired shell
#$ -S /bin/bash

#-q <queue> --- set the queue
#$ -q local.q

#-P <projectName> --- set the job's project
#-V --- will pass all environment variables to the job
#-v var[=value]  --- will specifically pass environment variable 'var' to the job
#-b y --- allow command to be a binary file instead of a script
#-w e --- verify options and abort if there is an error
#-N <jobname> : name of the job
#-l h_vmem=size --- specify the amount of memory required (e.g. 3G or 3500M) (NOTE: This is memory per processor slot. So if you ask for 2 processors total memory will be 2 X hvmem_value)
#-l h_rt=hh:mm:ss --- specify the maximum run time (hours, minutes and seconds)
#-l s_rt=hh:mm:ss --- specify the soft run time limit (hours, minutes and seconds) - Remember to set both s_rt and h_rt
#-pe shm <n_processors> --- run a parallel job using pthreads or other shared-memory API
#-cwd : Move to current working directory
#$ -cwd

#-wd <dir>  : Set working directory for this job
#-j [y/n] : whether you want to merge output and error log files
#-o <output_logfile>
#-e <error_logfile>
#-m beas :Will send email when job begins, ends, aborts, suspends
#-M <emailaddress> :Email address to send email to
#-t <start>-<end>:<incr> : submit a job array with start index <start>, stop index <end> in increments using <incr>
#$-t 1-3:1

#-hold_jid <comma separated list of job-ids, can also be a job id pattern such as 2722*> : will start the current job/job -array only after completion of all jobs in the comma separated list
#-hold_jid_ad <job array id, pattern or name>: will start the current job in a job array only after completion of corresponding job in the job array in <>


filename="run_simulation.py"

echo "I am job $JOB_ID.$SGE_TASK_ID"

cd ./jobs/$SGE_TASK_ID

echo "Working directory is:"
pwd

echo "Activating anaconda environment..."
source ~/anaconda3/bin/activate mbdoe-python

echo "Executing $filename..."
python "$filename"

echo "Moving logfiles (*$JOB_ID.$SGE_TASK_ID) to ./jobs/$SGE_TASK_ID/"
mv ../../*$JOB_ID.$SGE_TASK_ID ./
gzip *$JOB_ID.$SGE_TASK_ID
