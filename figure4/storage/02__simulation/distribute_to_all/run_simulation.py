####################### header version 2019-03-25 ####################
import hashlib
import io
import os
import sys

def sha256sum(src, digest_size_in_bytes=64, length=io.DEFAULT_BUFFER_SIZE):
    if sys.hexversion <= 0x030500f0:
        ## digest_size not settable before Python 3.5
        if digest_size_in_bytes != 64:
            print("Not possible to hash with digest_size different than 64 with your Python version.")
            sys.exit(2)
        sha2 = hashlib.new("sha256")
    else:
        sha2 = hashlib.new("sha256", digest_size=digest_size_in_bytes)
    with io.open(src, mode="rb") as fd:
        for chunk in iter(lambda: fd.read(length), b''):
            sha2.update(chunk)
    return sha2.hexdigest()

_mypath = os.path.abspath(__file__)
_mysha256sum = sha256sum(_mypath)
_myfilename = os.path.basename(__file__)

import logging
logger = logging.getLogger(__name__)

logFormatter = logging.Formatter("%(asctime)s [%(levelname)-5.5s]  %(message)s")
rootLogger = logging.getLogger()
rootLogger.setLevel(logging.INFO)

fileHandler = logging.FileHandler("{}.{}.log".format(_myfilename, sha256sum(_mypath)[:6]), mode="w")
fileHandler.setFormatter(logFormatter)
fileHandler.setLevel(logging.INFO)
rootLogger.addHandler(fileHandler)

consoleHandler = logging.StreamHandler()
consoleHandler.setFormatter(logFormatter)
consoleHandler.setLevel(logging.INFO)
rootLogger.addHandler(consoleHandler)


logger.info("Hi, this is: '{}' as '{}'".format(_myfilename, __name__))
logger.info("I am located here:")
logger.info(_mypath)
logger.info("My sha256sum hash is:")
logger.info(_mysha256sum)
######################################################################



import pandas
import json_tricks as pickler
import numpy
import mbdoe
import model as mod
import numpy
import copy
import math
from collections import OrderedDict
import csv
import matplotlib.pyplot as plt



mbdoe.enable_debug()
import os
try:
    os.mkdir("./figures/")
except:
    pass


m = mod.initialize()
opt = mbdoe.Optimization(m, True)
opt.set_solver_sundials()


extract_params = [
    "k_E_N_P",
    "k_E_N_P_2",
    "k_ENP_D",
    "k_ENP_D_2"
]
pars = pandas.read_csv("simulate.csv")
for key in extract_params:
    value = pars[key].iloc[0]
    print(key, value)
    setattr(opt.model, key, value)

metadata="metadata.csv"
suite = mbdoe.SuiteOfExperimentalData()
suite.read_experiments(metadata)
suite.calculate_and_store_all_modelled_data(opt)

df_list = []
for i, moddat in enumerate(suite.all_experiments_modelled):
    df = moddat.as_dataframe()
    serie = df[ ["t", "nucl1"]  ]
    serie[i] = serie["nucl1"]
    del(serie["nucl1"])
    serie = serie.set_index("t")
    df_list.append( serie )
#total_df = pandas.concat(df_list, join_axes)
total_df = df_list[0].join(df_list[1:], how="outer")
total_df.to_csv("out_nucl1.csv")

wlsq_df_storage = []
for index, e in enumerate(suite.all_experiments):
    metadata, experiment = e
    moddat = suite.all_experiments_modelled[index]
    expdat = experiment
    residual_obj = expdat.calculate_residuals_against_modelled_data(moddat)

    #with variance:
    residual_obj.matrix = residual_obj.matrix**2 / residual_obj.matrix_variance
    #with stderr:
    #residual_obj.matrix = residual_obj.matrix**2 / residual_obj.matrix_variance**0.5
    wlsq_df = residual_obj.as_dataframe()

    wlsq_df["wlsq_nucl1"] = wlsq_df["residual_nucl1"]
    wlsq_df = wlsq_df[["wlsq_nucl1"]]
    wlsq_df["t"] = wlsq_df.index
    wlsq_df["initial_nucl1"] = metadata.nucl1
    wlsq_df["initial_enz1"] = metadata.enz1
    wlsq_df["initial_phos"] = metadata.phos
    wlsq_df["description"] = metadata.description
    print(wlsq_df)
    wlsq_df_storage.append(wlsq_df)
wlsq_df_total = pandas.concat(wlsq_df_storage, ignore_index=True)
wlsq_df_total.to_csv("wlsq_df_total.csv")

import matplotlib.pyplot as plt


listing = [
    "t",
    "initial_nucl1",
    "initial_enz1",
    "initial_phos",
]

for x in listing:
    y = "wlsq_nucl1"
    for z in listing:
        if x == z:
            continue
        c =  wlsq_df_total[z]
        fig = plt.figure()
        plt.scatter(wlsq_df_total[x], wlsq_df_total[y], c = c)
        plt.xlabel(x)
        plt.ylabel(y)
        cbar = plt.colorbar()
        #cbar.ax.set_yticklabels([])
        cbar.set_label(z) #, rotation=270)
        fig.savefig("./figures/scatter_{}--{}--{}.svg".format(x,y,z))
        plt.close(fig)


FIM = suite.calculate_FIM_when_modelled_data_available(opt)

acrit = opt.calculate_A_criterion(FIM)
print(acrit)
with open("acrit.txt","w") as f:
    f.write(str(acrit))

dcrit = opt.calculate_D_criterion(FIM)
print(dcrit)
with open("dcrit.txt","w") as f:
    f.write(str(dcrit))


import numpy
fim_inv = numpy.linalg.inv(FIM)
ecrit = max(numpy.linalg.eigvals(fim_inv))
print(ecrit)
with open("ecrit.txt", "w") as f:
    f.write(str(ecrit))




try:
    cov_matrix = numpy.linalg.inv(FIM)
    print("--- FULL COVARIANCE MATRIX ---")
    print cov_matrix
    # Plot confidence Ellipses from FIM
    conf_ellipses = opt.plot_confidence_ellipses(FIM, spacer=5)
    # Save matplot plot from confidence Ellipses in file
    fig = conf_ellipses[0]
    fig.savefig("./figures/conf.svg")
    plt.close("all")
except numpy.linalg.LinAlgError:
    print("FIM is singular")



fig2 = list(suite.plot_with_modelled_data_subplots(opt))
for index, (fig, axarray) in enumerate(fig2):
    fig.savefig("./figures/plot_nucleoside_only_"+str(index)+".svg")
    plt.close("all")

fig3 = list(suite.plot_with_modelled_data(opt))
for index, fig_new in enumerate(fig3):
    current_fig = fig_new[0]
    try:
        current_fig.tight_layout()
    except (TypeError, ValueError):
        pass
    current_fig.savefig("./figures/plot_all_statevariables_{}.svg".format(index))
    plt.close("all")



with open("lsq.txt", "w") as f:
    f.write(str(suite.calculate_total_cost_function_value_when_modelled_data_available(method="LSQ")))

with open("wlsq.txt", "w") as f:
    f.write(str(suite.calculate_total_cost_function_value_when_modelled_data_available(method="WLSQ")))

with open("rmsd.txt", "w") as f:
    f.write(str(suite.calculate_total_cost_function_value_when_modelled_data_available(method="RMSD")))

print(fim_inv)
