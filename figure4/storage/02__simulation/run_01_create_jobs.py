####################### header version 2019-03-25 ####################
import hashlib
import io
import os
import sys

def sha256sum(src, digest_size_in_bytes=64, length=io.DEFAULT_BUFFER_SIZE):
    if sys.hexversion <= 0x030500f0:
        ## digest_size not settable before Python 3.5
        if digest_size_in_bytes != 64:
            print("Not possible to hash with digest_size different than 64 with your Python version.")
            sys.exit(2)
        sha2 = hashlib.new("sha256")
    else:
        sha2 = hashlib.new("sha256", digest_size=digest_size_in_bytes)
    with io.open(src, mode="rb") as fd:
        for chunk in iter(lambda: fd.read(length), b''):
            sha2.update(chunk)
    return sha2.hexdigest()

_mypath = os.path.abspath(__file__)
_mysha256sum = sha256sum(_mypath)
_myfilename = os.path.basename(__file__)

import logging
logger = logging.getLogger(__name__)

logFormatter = logging.Formatter("%(asctime)s [%(levelname)-5.5s]  %(message)s")
rootLogger = logging.getLogger()
rootLogger.setLevel(logging.INFO)

fileHandler = logging.FileHandler("{}.{}.log".format(_myfilename, sha256sum(_mypath)[:6]), mode="w")
fileHandler.setFormatter(logFormatter)
fileHandler.setLevel(logging.INFO)
rootLogger.addHandler(fileHandler)

consoleHandler = logging.StreamHandler()
consoleHandler.setFormatter(logFormatter)
consoleHandler.setLevel(logging.INFO)
rootLogger.addHandler(consoleHandler)


logger.info("Hi, this is: '{}' as '{}'".format(_myfilename, __name__))
logger.info("I am located here:")
logger.info(_mypath)
logger.info("My sha256sum hash is:")
logger.info(_mysha256sum)
######################################################################


import os

print("I am here:")
print(os.getcwd())



#!/usr/bin/env python2
# -*- coding: utf-8 -*-


print("I am here:")
print(os.getcwd())
os.chdir("./files/02__simulation")
print("changed to:")
print(os.getcwd())
###


import os
import glob
if not os.path.isdir("jobs"):
    os.mkdir("jobs")

import pandas
df = pandas.read_csv("input.csv")

for index, row in df.iterrows():
    print(index,row)

    index = int(row["Job"])

    if not os.path.isdir("jobs/{}".format(index)):
        os.mkdir("jobs/{}".format(index))

    row.to_frame().transpose().to_csv("jobs/{}/simulate.csv".format(index) )

    os.symlink(os.path.abspath("../01__convert_to_excel/data/metadata_for_parameter_estimation.csv"), "./jobs/{}/metadata.csv".format(index))

    if not os.path.isdir("jobs/{}/data".format(index)):
        os.mkdir("jobs/{}/data".format(index))
    for file_to_link in glob.glob("../01__convert_to_excel/data/*.xls"):
        file_to_link = os.path.abspath(file_to_link)
        only_basename_of_file = os.path.basename(file_to_link)
        os.symlink(file_to_link, "jobs/{}/data/{}".format(index,only_basename_of_file) )

    for file_to_link in glob.glob("./distribute_to_all/*"):
        file_to_link = os.path.abspath(file_to_link)
        only_basename_of_file = os.path.basename(file_to_link)
        os.symlink(file_to_link, "jobs/{}/{}".format(index,only_basename_of_file))

print("Done with preparation of job directories.")

###
print("I am here:")
print(os.getcwd())
os.chdir("../../")
print("changed to:")
print(os.getcwd())
