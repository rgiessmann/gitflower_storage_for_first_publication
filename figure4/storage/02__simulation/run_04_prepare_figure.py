####################### header version 2019-03-25 ####################
import hashlib
import io
import os
import sys

def sha256sum(src, digest_size_in_bytes=64, length=io.DEFAULT_BUFFER_SIZE):
    if sys.hexversion <= 0x030500f0:
        ## digest_size not settable before Python 3.5
        if digest_size_in_bytes != 64:
            print("Not possible to hash with digest_size different than 64 with your Python version.")
            sys.exit(2)
        sha2 = hashlib.new("sha256")
    else:
        sha2 = hashlib.new("sha256", digest_size=digest_size_in_bytes)
    with io.open(src, mode="rb") as fd:
        for chunk in iter(lambda: fd.read(length), b''):
            sha2.update(chunk)
    return sha2.hexdigest()

_mypath = os.path.abspath(__file__)
_mysha256sum = sha256sum(_mypath)
_myfilename = os.path.basename(__file__)

import logging
logger = logging.getLogger(__name__)

logFormatter = logging.Formatter("%(asctime)s [%(levelname)-5.5s]  %(message)s")
rootLogger = logging.getLogger()
rootLogger.setLevel(logging.INFO)

#fileHandler = logging.FileHandler("{}.{}.log".format(_myfilename, sha256sum(_mypath)[:6]), mode="w")
#fileHandler.setFormatter(logFormatter)
#fileHandler.setLevel(logging.INFO)
#rootLogger.addHandler(fileHandler)

consoleHandler = logging.StreamHandler()
consoleHandler.setFormatter(logFormatter)
consoleHandler.setLevel(logging.INFO)
rootLogger.addHandler(consoleHandler)


logger.info("Hi, this is: '{}' as '{}'".format(_myfilename, __name__))
logger.info("I am located here:")
logger.info(_mypath)
logger.info("My sha256sum hash is:")
logger.info(_mysha256sum)
######################################################################



print("I am here:")
print(os.getcwd())

os.chdir("./files/02__simulation/")

print("...changed to:")
print(os.getcwd())



import glob
import pandas


import matplotlib
#matplotlib.use('Agg',warn=False)
import matplotlib.pyplot as plt
plt.ioff()
plt.switch_backend("agg")



datacsv = glob.glob("jobs/4/out_nucl1_experimental.csv")
print(datacsv)
data_dfs = [pandas.read_csv(d) for d in datacsv]

expcsv = ["jobs/1/out_nucl1.csv","jobs/2/out_nucl1.csv","jobs/3/out_nucl1.csv"]
exp_dfs = [pandas.read_csv(d) for d in expcsv]

how_many_experiments = len(pandas.read_csv(datacsv[0]).columns) - 1
for expname in [41]: #range(how_many_experiments):

    fig = plt.figure(figsize=(8 /2.54, 12 /2.54), dpi=600)
    ax = plt.subplot(211)

    for i, data_df in enumerate(data_dfs):

        bar = list([foo for foo in data_df.columns if foo[foo.rfind("_")+1:] == str(expname)])
        print(bar)
        assert(len(bar) == 1)
        selected_column_name = bar[0]
        miniframe = data_df[["t", selected_column_name]]
        miniframe = miniframe.dropna(axis="index")
        miniframe.t /= 60.0

        t0 = miniframe.query("t == 0")
        miniframe["variance"] = 1.96 * 0.05 * float(t0[selected_column_name])

        ax.errorbar(miniframe["t"], miniframe[selected_column_name], miniframe["variance"], capsize=4, marker="s", markeredgecolor="black", markeredgewidth=0.5, elinewidth=2, linewidth=0, label="experimental", zorder=20)

    for i, data_df in enumerate(exp_dfs):
        bar = list([foo for foo in data_df.columns if foo[foo.rfind("_")+1:] == str(expname)])
        assert(len(bar) == 1)
        selected_column_name = bar[0]
        miniframe = data_df[["t", selected_column_name]]
        miniframe = miniframe.dropna(axis="index")
        miniframe.t /= 60.0

        given_data = expcsv[i]
        offset_left  = given_data.rfind("jobs/") + len("jobs/")
        offset_right = given_data.rfind("/out_nucl1.csv")
        job_number   = given_data[offset_left:offset_right]


        _label = ""
        if job_number == "1":
            _label = "k"
        if job_number == "2":
            _label = "k*"
        if job_number == "3":
            _label = "k**"

        ax.plot(miniframe["t"], miniframe[selected_column_name], linewidth=1, label=_label, zorder=10)

    ax.legend(loc=(0.35, 0.4), fontsize=10)
    plt.ylabel("nucleoside conc. [mM]")
    plt.xlabel("time [h]")
    ax.set_xticks([0,8,16,24])
    ax.set_xticks([0,2,4,6,8,10,12,14,16,18,20,22,24,26,28], minor=True)
    #plt.tight_layout()
    plt.ylim(top=5.5, bottom=0) #

    ax.text(0,-1, "Figure 4 Giessmann et al.", transform=ax.transAxes)

    fig.savefig("./Figure 4 Giessmann et al.pdf")
    fig.savefig("./Figure 4 Giessmann et al.png")

    plt.close(fig)




print("I am here:")
print(os.getcwd())

os.chdir("../../")

print("...changed to:")
print(os.getcwd())

