####################### header version 2019-04-03 ####################
import hashlib
import io
import os
import sys
import shutil

def sha256sum(src, digest_size_in_bytes=64, length=io.DEFAULT_BUFFER_SIZE):
    if sys.hexversion <= 0x030500f0:
        ## digest_size not settable before Python 3.5
        if digest_size_in_bytes != 64:
            print("Not possible to hash with digest_size different than 64 with your Python version.")
            sys.exit(2)
        sha2 = hashlib.new("sha256")
    else:
        sha2 = hashlib.new("sha256", digest_size=digest_size_in_bytes)
    with io.open(src, mode="rb") as fd:
        for chunk in iter(lambda: fd.read(length), b''):
            sha2.update(chunk)
    return sha2.hexdigest()

_mypath = os.path.abspath(__file__)
_mysha256sum = sha256sum(_mypath)
_myfilename = os.path.basename(__file__)

shutil.copyfile(_mypath, "{}.{}".format(_myfilename, sha256sum(_mypath)[:6]))

import logging
logger = logging.getLogger(__name__)

logFormatter = logging.Formatter("%(asctime)s [%(levelname)-5.5s]  %(message)s")
rootLogger = logging.getLogger()
rootLogger.setLevel(logging.INFO)

fileHandler = logging.FileHandler("{}.{}.log".format(_myfilename, sha256sum(_mypath)[:6]), mode="w")
fileHandler.setFormatter(logFormatter)
fileHandler.setLevel(logging.INFO)
rootLogger.addHandler(fileHandler)

consoleHandler = logging.StreamHandler()
consoleHandler.setFormatter(logFormatter)
consoleHandler.setLevel(logging.INFO)
rootLogger.addHandler(consoleHandler)


logger.info("Hi, this is: '{}' as '{}'".format(_myfilename, __name__))
logger.info("I am located here:")
logger.info(_mypath)
logger.info("My sha256sum hash is:")
logger.info(_mysha256sum)
######################################################################



import pandas
import pickle
import numpy
import mbdoe
import model as mod
import numpy
import copy
import math
from collections import OrderedDict
import csv
import matplotlib.pyplot as plt


import time

mbdoe.enable_debug()

import os
try:
    os.mkdir("./figures/")
except:
    pass


m = mod.initialize()
opt = mbdoe.Optimization(m, True)
opt.set_solver_sundials()

metadata="metadata.csv"
suite = mbdoe.SuiteOfExperimentalData()
suite.read_experiments(metadata)

import lmfit
pars = lmfit.Parameters()
pars.add("k_E_N_P",   min=0.00)
pars.add("k_E_N_P_2", min=0.00)
pars.add("k_ENP_D",   min=0.00)
pars.add("k_ENP_D_2", min=0.00)


eps = 1e-10
simu = pandas.read_csv("simulate.csv")
row = simu.iloc[0]
for key in pars.keys():
    _value = float(simu[key])
    pars[key].set(value = _value)

    _vary  = True
    pars[key].set(vary  = _vary )

    pars[key].set(min = _value - eps, max = _value + eps)

print(pars.pretty_print())
pars.pretty_print()
logger.info(pars.pretty_print())


def residual(p):
    for key in pars.keys():
        value = p[key].value
        logger.info(key)
        logger.info(value)
        setattr(opt.model, key, value)

    suite.calculate_and_store_all_modelled_data(opt)
    residual_storage = suite.return_residual_objects_per_experiment_when_modelled_data_available()

    reduced_list = []

    if True:
        residual_method = "WLSQ"
        if residual_method == "WLSQ":
            for residuals in residual_storage:
                number_of_elements = len(residuals.matrix.flat)
                for i in range(number_of_elements):
                    diff = residuals.matrix.flat[i]
                    var  = residuals.matrix_variance.flat[i]
                    if not numpy.isnan(diff):
                        value = (1/var)**(0.5) * diff ## **0.5 because residual will be squared by least_squares function
                        reduced_list.append(value)

    return reduced_list


def least_squares_jacobian(parameter_value_array, apply_bounds_transformation, *args, **kwargs):
    print("least_squares_jacobian")
    logger.info("least_squares_jacobian")
    print(args)
    print(kwargs)
    gradient = suite.return_residuals_gradient_for_optimization(parameter_value_array, opt, pars.keys(), residual_method="WLSQ")
    return gradient



if not os.path.isdir("itercb"):
    os.mkdir("itercb")
class IterTime:
    pass
iter_time = IterTime()
iter_time.t = time.time()

def callback_iter(params, iter, resid, *fcn_args, **fcn_kws):
    print("callback_iter")
    logger.info("callback_iter")

    params.pretty_print()
    print(params.dumps())
    logger.info(params.dumps())

    r = sum([r**2 for r in resid])

    FIM = suite.calculate_FIM_when_modelled_data_available(opt)

    import numpy
    acrit = opt.calculate_A_criterion(FIM)
    dcrit = opt.calculate_D_criterion(FIM)
    try:
        fim_inv = numpy.linalg.inv(FIM)
        ecrit = max(numpy.linalg.eigvals(fim_inv))
    except numpy.linalg.LinAlgError as err:
        ecrit = None

    if iter == -1:
        iter_time.t = time.time()
        f= open("itercb/iter_cb_{}.csv".format(iter_time.t), "w")
    else:
        f= open("itercb/iter_cb_{}.csv".format(iter_time.t), "a")
    f.write(",".join( [str(iter), str(r)] + list([str(params[key].value) for key in params.keys()]) ))
    f.write(",")
    f.write(",".join( [str(crit) for crit in [acrit, dcrit, ecrit]] ))
    f.write("\n")
    f.close()

    print("iter: {}, residual: {}, crits: {} params: {}".format(iter, r, [acrit,dcrit,ecrit], params.valuesdict()))
    logger.debug("iter: {}, residual: {}, crits: {} params: {}".format(iter, r, [acrit,dcrit,ecrit], params.valuesdict()))

    return



iter_time = IterTime()
iter_time.t = time.time()

minimizer = lmfit.Minimizer(residual, pars, iter_cb = callback_iter)
minimizer_result = minimizer.minimize(method='least_squares', jac = least_squares_jacobian)
#minimizer_result = minimizer.minimize(method="L-BFGS-B", jac= lbfgsb_jacobian)
#minimizer_result = minimizer.minimize(method='basinhopping',
#                minimizer_kwargs = {"method": "L-BFGS-B", "jac": lbfgsb_jacobian},
#                callback = callback_basinhop)

import pickle
import time
curtime = time.time()
with open("minimizerresult_lmfit_{}.pickle".format(curtime),"wb") as f:
    pickle.dump(minimizer_result, f)

with open("minimizerresult_lmfit_{}.pickle".format(curtime),"rb") as f:
    minimizer_result = pickle.load(f)


print("")
print("")
print("--- MINIMIZER RESULT ---")
print(lmfit.fit_report(minimizer_result))
logger.info(lmfit.fit_report(minimizer_result))
print("------------------------")
print(lmfit.fit_report(minimizer_result.params))
logger.info(lmfit.fit_report(minimizer_result.params))
print("")
print("")
