#!/usr/bin/env python2
# -*- coding: utf-8 -*-

import os

print("I am here:")
print(os.getcwd())
os.chdir("./files/02__parameter_estimation/")
print("changed to:")
print(os.getcwd())


import importlib.util
spec = importlib.util.spec_from_file_location("run_01_create_jobs", "./run_01_create_jobs.py")
module = importlib.util.module_from_spec(spec)
spec.loader.exec_module(module)


print("I am here:")
print(os.getcwd())


import subprocess
subprocess.run('chmod u+x parest_starter.sh', shell=True)
subprocess.run('/bin/bash -c "./parest_starter.sh"', shell=True)


print("I am here:")
print(os.getcwd())
os.chdir("../../")
print("changed to:")
print(os.getcwd())
