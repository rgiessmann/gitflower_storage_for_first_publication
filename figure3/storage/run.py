import pyblake2
import io
import os

import logging
logFormatter = logging.Formatter("%(asctime)s [%(levelname)-5.5s]  %(message)s")
rootLogger = logging.getLogger()
rootLogger.setLevel(logging.INFO)

fileHandler = logging.FileHandler("run.log", mode="w")
fileHandler.setFormatter(logFormatter)
fileHandler.setLevel(logging.INFO)
rootLogger.addHandler(fileHandler)

consoleHandler = logging.StreamHandler()
consoleHandler.setFormatter(logFormatter)
consoleHandler.setLevel(logging.INFO)
rootLogger.addHandler(consoleHandler)



def self_log():
    ## Log myself with hash value
    def b2sum(src, digest_size_in_bytes=64, length=io.DEFAULT_BUFFER_SIZE):
        """Calculates a BLAKE2 hash sum of a file, given as file name.
        Returns the b2sum.
        """
        b2 = pyblake2.blake2b(digest_size=digest_size_in_bytes)
        with io.open(src, mode="rb") as fd:
            for chunk in iter(lambda: fd.read(length), b''):
                b2.update(chunk)
        return b2.hexdigest()
    path = os.path.abspath(__file__)
    logger = logging.getLogger(__name__)
    logger.info(f"Hi, this is: '{__name__}'")
    logger.info("I am located here:")
    logger.info(path)
    logger.info("My b2sum hash is:")
    logger.info(b2sum(path))
self_log()



import os

print("I am here:")
print(os.getcwd())


import importlib.util
spec = importlib.util.spec_from_file_location("00__fit_raw_spectral_data", "./files/00__fit_raw_spectral_data/00_run_fit_raw_spectral_data.py")
module = importlib.util.module_from_spec(spec)
spec.loader.exec_module(module)


print("I am here:")
print(os.getcwd())


import importlib.util
spec = importlib.util.spec_from_file_location("01__convert_to_excel", "./files/01__convert_to_excel/01_run_convert_to_excel.py")
module = importlib.util.module_from_spec(spec)
spec.loader.exec_module(module)

print("I am here:")
print(os.getcwd())


import importlib.util
spec = importlib.util.spec_from_file_location("02__parameter_estimation", "./files/02__parameter_estimation/parest_starter.py")
module = importlib.util.module_from_spec(spec)
spec.loader.exec_module(module)

print("I am here:")
print(os.getcwd())


import importlib.util
spec = importlib.util.spec_from_file_location("04__simulate_and_plot", "./files/04__simulate_and_plot/simplot_starter.py")
module = importlib.util.module_from_spec(spec)
spec.loader.exec_module(module)

print("I am here:")
print(os.getcwd())
