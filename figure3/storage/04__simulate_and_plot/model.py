def initialize(include_sensitivities=True):
    import mbdoe
    m = mbdoe.Problem()
    m.calculate_sensitivities = include_sensitivities

    state_variables = """
    phos
    nucl1
    enz1_comp_ENP
    enz1
    base1
    R1P
    """

    parameters = """
    k_E_N_P_normalized
    k_E_N_P_2_normalized
    k_ENP_D_normalized
    k_ENP_D_2_normalized
    """

    time_invariant_input_controls = """
    k_E_N_P
    k_E_N_P_2
    k_ENP_D
    k_ENP_D_2
    """

    ## add items above to global environment
    for item in state_variables.splitlines():
        item = item.strip()
        if item == "":
            continue
        function = "m.add_state_variable"
        exec (str(item) + " = " + function + "('" + str(item) + "')")

    for item in parameters.splitlines():
        item = item.strip()
        if item == "":
            continue
        function = "m.add_parameter"
        exec (str(item) + " = " + function + "('" + str(item) + "')")

    for item in time_invariant_input_controls.splitlines():
        item = item.strip()
        if item == "":
            continue
        function = "m.add_time_invariant_input_control"
        exec (str(item) + " = " + function + "('" + str(item) + "')")

    # define abbreviations
    m.k_E_N_P_normalized     = 1
    m.k_E_N_P_2_normalized   = 1
    m.k_ENP_D_normalized     = 1
    m.k_ENP_D_2_normalized   = 1

    k_E_N_P   = k_E_N_P   * k_E_N_P_normalized
    k_E_N_P_2 = k_E_N_P_2 * k_E_N_P_2_normalized
    k_ENP_D   = k_ENP_D   * k_ENP_D_normalized
    k_ENP_D_2 = k_ENP_D_2 * k_ENP_D_2_normalized


    r1   = k_E_N_P     * enz1           * nucl1 * phos
    r1_2 = k_E_N_P_2   * enz1_comp_ENP
    r2   = k_ENP_D     * enz1_comp_ENP
    r2_2 = k_ENP_D_2   * enz1           * base1 * R1P

    # define ODE system

    ode = dict()

    ode[nucl1]          = - r1 + r1_2
    ode[phos]           = - r1 + r1_2
    ode[base1]          = + r2 - r2_2
    ode[enz1_comp_ENP]  = + r1 - r1_2 - r2 + r2_2
    ode[enz1]           = - r1 + r1_2 + r2 - r2_2
    ode[R1P]            = + r2 - r2_2

    ## register the ode system
    for eq in ode:
        m.add_differential_equation(eq, ode[eq])

    m.initialize_symbolic_matrices()
    m.initialize_lambdified_matrices()

    return m
