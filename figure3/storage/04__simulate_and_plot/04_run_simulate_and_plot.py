####################### header version 2019-04-03 ####################
import hashlib
import io
import os
import sys
import shutil

def sha256sum(src, digest_size_in_bytes=64, length=io.DEFAULT_BUFFER_SIZE):
    if sys.hexversion <= 0x030500f0:
        ## digest_size not settable before Python 3.5
        if digest_size_in_bytes != 64:
            print("Not possible to hash with digest_size different than 64 with your Python version.")
            sys.exit(2)
        sha2 = hashlib.new("sha256")
    else:
        sha2 = hashlib.new("sha256", digest_size=digest_size_in_bytes)
    with io.open(src, mode="rb") as fd:
        for chunk in iter(lambda: fd.read(length), b''):
            sha2.update(chunk)
    return sha2.hexdigest()

_mypath = os.path.abspath(__file__)
_mysha256sum = sha256sum(_mypath)
_myfilename = os.path.basename(__file__)

shutil.copyfile(_mypath, "{}.{}".format(_myfilename, sha256sum(_mypath)[:6]))

import logging
logger = logging.getLogger(__name__)

logFormatter = logging.Formatter("%(asctime)s [%(levelname)-5.5s]  %(message)s")
rootLogger = logging.getLogger()
rootLogger.setLevel(logging.INFO)

fileHandler = logging.FileHandler("{}.{}.log".format(_myfilename, sha256sum(_mypath)[:6]), mode="w")
fileHandler.setFormatter(logFormatter)
fileHandler.setLevel(logging.INFO)
rootLogger.addHandler(fileHandler)

consoleHandler = logging.StreamHandler()
consoleHandler.setFormatter(logFormatter)
consoleHandler.setLevel(logging.INFO)
rootLogger.addHandler(consoleHandler)


logger.info("Hi, this is: '{}' as '{}'".format(_myfilename, __name__))
logger.info("I am located here:")
logger.info(_mypath)
logger.info("My sha256sum hash is:")
logger.info(_mysha256sum)
######################################################################




import os

logger = logging.getLogger(__name__)

logger.info("Starting the plotting...")



import json_tricks as pickler
import matplotlib
import matplotlib.pyplot as plt
import numpy
import mbdoe
import model as mod
import numpy
import copy
import math

from collections import OrderedDict

# Enabling debug mode for diagnosis
mbdoe.enable_debug()

plt.style.use("default")



extract_params = ["k_E_N_P", "k_E_N_P_2", "k_ENP_D", "k_ENP_D_2"]

parameter_estimation_results_file = "results.json"



m = mod.initialize()
opt = mbdoe.Optimization(m, True)
opt.set_solver_sundials()

with open(parameter_estimation_results_file, "r") as f:
    parameter_values = OrderedDict(pickler.load(f))

for key in extract_params:
    setattr(opt.model, key, parameter_values[key])

fig = plt.figure(figsize=(16 /2.54, 16 /2.54), dpi=600)
fig, [ax_left, ax_right] = plt.subplots(nrows=1, ncols=2, sharex = True, sharey = True)
plt.subplots_adjust(bottom=0.5)
## start to plot all data in the metadata file ...
#ax_left = plt.subplot(121)



#########
## plot right graph
#########

metadata="metadata_right.csv"
suite = mbdoe.SuiteOfExperimentalData()
suite.read_experiments(metadata)
suite.calculate_and_store_all_modelled_data(opt)

_how_many_experiments_to_plot = len(suite.all_experiments)

color = ["red", "blue"]
color = [list(matplotlib.colors.to_rgba(c)) for c in color]

ax_right.set_title("High phosphate")

i=0
for (_meta_exp, experimental_data), modelled_data in zip(suite.all_experiments, suite.all_experiments_modelled):
    _c = color[i]

    mod_df = modelled_data.as_dataframe()
    mod_df["t"] /=60

    ax_right.errorbar(x=mod_df["t"], y=mod_df["nucl1"], yerr=None                , fmt="-", label="", c=_c, linewidth=3)
    i+=1

i=0
for (_meta_exp, experimental_data), modelled_data in zip(suite.all_experiments, suite.all_experiments_modelled):
    _c = color[i]
    _lighter_color = copy.copy(_c)
    _lighter_color[3] = 0.5

    exp_df = experimental_data.as_dataframe()
    exp_df["95ci_error"] = 1.96* exp_df["variance_nucl1"].apply(math.sqrt)
    exp_df["t"] = exp_df.index
    exp_df["t"] /= 60

    print(_meta_exp["description"])
    ax_right.errorbar(x=exp_df["t"], y=exp_df["nucl1"], yerr=exp_df["95ci_error"], fmt="s", label=_meta_exp["description"], c=_c, markersize=4, capsize=3, ecolor=_lighter_color, markeredgecolor="black", markeredgewidth=0.5, elinewidth=2, linewidth=0)
    i+=1





#########
## plot left graph
#########


metadata="metadata_left.csv"
suite = mbdoe.SuiteOfExperimentalData()
suite.read_experiments(metadata)
suite.calculate_and_store_all_modelled_data(opt)

_how_many_experiments_to_plot = len(suite.all_experiments)

color = ["red", "blue"]
color = [list(matplotlib.colors.to_rgba(c)) for c in color]
ax_left.set_title("Low phosphate")

i=0
for (_meta_exp, experimental_data), modelled_data in zip(suite.all_experiments, suite.all_experiments_modelled):
    _c = color[i]

    mod_df = modelled_data.as_dataframe()
    mod_df["t"] /=60

    ax_left.errorbar(x=mod_df["t"], y=mod_df["nucl1"], yerr=None                , fmt="-", label="", c=_c, linewidth=3)
    i+=1

i=0
for (_meta_exp, experimental_data), modelled_data in zip(suite.all_experiments, suite.all_experiments_modelled):
    _c = color[i]
    _lighter_color = copy.copy(_c)
    _lighter_color[3] = 0.5

    exp_df = experimental_data.as_dataframe()
    exp_df["95ci_error"] = 1.96* exp_df["variance_nucl1"].apply(math.sqrt)
    exp_df["t"] = exp_df.index
    exp_df["t"] /= 60

    print(_meta_exp["description"])
    ax_left.errorbar(x=exp_df["t"], y=exp_df["nucl1"], yerr=exp_df["95ci_error"], label=_meta_exp["description"], fmt="s", c=_c, markersize=4, capsize=3, ecolor=_lighter_color, markeredgecolor="black", markeredgewidth=0.5, elinewidth=2, linewidth=0)
    i+=1

#ax_left.legend()
## ... end.










#ax_right.legend()



#ax.legend(loc=(0.35, 0.4), fontsize=10)

ax_left.set_ylabel("nucleoside conc. [mM]")
ax_left.set_xlabel("time [h]")
ax_right.set_xlabel("time [h]")
ax_left.set_xticks([0,8,16,24])
ax_right.set_xticks([0,8,16,24])
ax_left.set_xticks([0,2,4,6,8,10,12,14,16,18,20,22,24,26,28], minor=True)
ax_right.set_xticks([0,2,4,6,8,10,12,14,16,18,20,22,24,26,28], minor=True)
#plt.tight_layout()
plt.ylim(top=5.5, bottom=0) #

ax_left.text(0,-1, "Figure 3 Giessmann et al.", transform=ax_left.transAxes)



fig.savefig("Figure 3 Giessmann et al.pdf")
fig.savefig("Figure 3 Giessmann et al.png")
